#!/bin/bash

## Use ci as working directory
cd ci

## Check environment
env=$1
if [[ -z ${env} ]]; then
    echo "Environment must be specified"
    exit 1
elif [[ ! -f ${env}.env ]]; then
    echo "No environment file for $env specified"
    exit 2
fi

## Copy files to a "docker-compose" directory
dc_dir=/var/docker/compose/stuen.it/${env}
mkdir -p ${dc_dir}
cp docker-compose.yml ${dc_dir}
cp ${env}.env ${dc_dir}/.env

## Deploy using docker-compose
cd ${dc_dir}
echo ${CI_BUILD_TOKEN} | docker login -u gitlab-ci-token --password-stdin registry.gitlab.com
docker-compose pull
docker-compose up -d